# Oowlish Test - Pet Breeds

The purppose of this repository is about a pratice test from the Oowlish company consisting a **Wordpress website which lists all registered animal breeds** on system, and **consult each breeds registered to see your details**, and a **form to submit experiences** about the selected breed to share with the users.

# Getting started

It's necessary that the environment has the **docker** installed with the **docker-compose** to generate the virtual machines that will run the website. It's also necessary that the environment that will be installed is enabled virtualization in order to install the docker.


# Installing the docker (if you don't have the docker installed)

`sudo apt-get install docker`

`sudo apt-get install docker-compose`

`sudo service docker start`

# Installing the environment 

With the docker installed, run the code bellow in terminal:

`git clone git@gitlab.com:zZJoelZz/petshop-oowlish.git`

`docker-compose up -d`

After installation, the environment'll configured and ready to use.

*  [http://localhost:8001/](http://localhost:8001/) (Will open the Wordpress website)
    
*  [http://localhost:8000/](http://localhost:8000/) (Will open the Phpmyadmin, if you need to acess the database)
    *  Username: root
    *  Password: test

# In WordPress

To acess the admin area of wordpress acess [http://localhost:8001/wp-admin](http://localhost:8001/wp-admin)

*  Username: admin
*  Password: admin

# Troubleshooting

>  After install the environment, on acess http://localhost:8001, is showing error of permission denied, or mysqli_real_connect()

To solve this error run in terminal in the current directory of the project:

`chmod 777 -R .*`

This command will do the permissions to acess the system between the docker containers.





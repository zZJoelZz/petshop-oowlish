<?php
/**
 * Template part for displaying list of breeds.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */

?>

    <script>
        /*jquery for wordpress*/
        jQuery(document).ready(function($) {

            var id_post = <?php echo get_the_id(); ?>;
            $(".breed-type .ap-pro-textfield").val(id_post);
        });
    </script>

    <?php 
$breed_title = get_the_title();
?>
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <?php if ( ! twentynineteen_can_show_post_thumbnail() ) : ?>
                <header class="entry-header">
                    <?php get_template_part( 'template-parts/header/entry', 'header' ); ?>
                </header>
                <?php endif; ?>

                    <div class="container">
                        <div class="row">

                            <div class="col-md-7">
                                <?php
		the_content(
			sprintf(
				wp_kses(
					/* translators: %s: Post title. Only visible to screen readers. */
					__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'twentynineteen' ),
					array(
						'span' => array(
							'class' => array(),
						),
					)
				),
				get_the_title()
			)
		);

		wp_link_pages(
			array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'twentynineteen' ),
				'after'  => '</div>',
			)
        );

		?>
                            </div>

                            <div class="col-md-5">

                                <?php 

$images = get_field('gallery');

if( $images ): ?>
                                    <div class="gallery-list">
                                        <?php foreach( $images as $image ): ?>
                                            <div class="item">
                                                <a href="<?php echo $image['url']; ?>">
                     <img src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" />
                </a>
                                                <p>
                                                    <?php echo $image['caption']; ?>
                                                </p>
                                            </div>
                                            <?php endforeach; ?>
                                    </div>
                                    <?php endif; ?>

                            </div>
                        </div>

                    </div>
                    <!-- .entry-content -->

                    <!--Testimonials of owners of the current breed -->

                    <?php

                $args = array(
                    'post_type' => 'testimonial',
                   'meta_key'		=> 'breed',
	'meta_value'	=> get_the_ID()

);
$the_query = new WP_Query( $args );
 if ( $the_query->have_posts() ) : ?>

                        <!-- the loop -->
                        <div class="testimonials-section">
                            <div class="container">
                                <h2 class="" style="text-align: center;">Owners testimonials for <?php the_title(); ?> </h2>
                                <hr>
                                <div class="">

                                    <div class="testimonials-list">

                                        <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                                            <div class="item" style="padding: 20px;">
                                                <div class="item breed-item">

                                                    <div>
                                                        <p class="owner-testimonal">
                                                            <?php echo get_post_meta( get_the_ID(), 'description', true ) ?>
                                                        </p>
                                                        <p class="owner-name"><strong><?php the_title(); ?> - <?php echo get_post_meta( get_the_ID(), 'pet_name', true ) ?>'s Owner</strong></p>

                                                    </div>

                                                </div>
                                            </div>

                                            <?php endwhile; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end of the loop -->

                        <?php else : ?>
                            <p>
                                <?php _e( '' ); ?>
                            </p>
                            <?php endif; ?>

                                <!--Bleeds testimonial front-end form-->
                                <div class="container">
                                    <h2 style="text-align: center;">Do you have any experiences about <?php echo $breed_title; ?>? Share with us. </h2>
                                    <hr>
                                    <?php echo do_shortcode('[ap-form id="1"]')?>
                                </div>

                                <footer class="entry-footer">
                                    <?php twentynineteen_entry_footer(); ?>
                                </footer>
                                <!-- .entry-footer -->

                                <?php if ( ! is_singular( 'attachment' ) ) : ?>
                                    <?php get_template_part( 'template-parts/post/author', 'bio' ); ?>
                                        <?php endif; ?>

        </article>
        <!-- #post-<?php the_ID(); ?> -->
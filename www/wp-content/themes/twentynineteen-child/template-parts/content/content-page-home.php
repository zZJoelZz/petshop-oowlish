<?php
/**
 * Template part for displaying page content in page-home.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */

?>


<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php if ( ! twentynineteen_can_show_post_thumbnail() ) : ?>
	<header class="entry-header">
		<?php get_template_part( 'template-parts/header/entry', 'header' ); ?>
	</header>
	<?php endif; ?>

	<div class="entry-content">
		<?php
		the_content();
		?>
	</div><!-- .entry-content -->


<!--All Breeds List -->
  <?php
                $args = array(
                    'post_type' => 'breed',
                    'posts_per_page' => '-1',
);
$the_query = new WP_Query( $args );
 if ( $the_query->have_posts() ) : ?>
                
 <div class="container">
 
 <div class="row">

 		<!-- the loop -->
                <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                <div class="col-md-4 ajust-height" style="padding-bottom: 20px;">
                    <div class="breeds-grid">
                    <div>
                    <a href="<?php  echo get_permalink(); ?>"  target="">
                        <?php echo the_post_thumbnail( 'large' );  ?>
                        </a>
                    </div>
                        <div class="breeds-data">
                        
                <h4> <a href="<?php echo get_permalink(); ?>"  target=""><?php the_title(); ?></a></h4>
                <?php the_excerpt(); ?>
                    <a href="<?php echo get_permalink(); ?>" class="button read-more" target="">Read More</a>
                        </div>
                </div>
                </div>

                <?php endwhile; ?>

 </div>
          <!-- end of the loop -->
          
     </div>

                

                <?php wp_reset_postdata(); ?>

                <?php else : ?>
                <p><?php _e( "sorry, but we don't have some breeds right now. Try again Later" ); ?></p>
                <?php endif; ?>



	<?php if ( get_edit_post_link() ) : ?>
		<footer class="entry-footer">
			<?php
			edit_post_link(
				sprintf(
					wp_kses(
						/* translators: %s: Post title. Only visible to screen readers. */
						__( 'Edit <span class="screen-reader-text">%s</span>', 'twentynineteen' ),
						array(
							'span' => array(
								'class' => array(),
							),
						)
					),
					get_the_title()
				),
				'<span class="edit-link">' . twentynineteen_get_icon_svg( 'edit', 16 ),
				'</span>'
			);
			?>
		</footer><!-- .entry-footer -->
	<?php endif; ?>
</article><!-- #post-<?php the_ID(); ?> -->